const graphql = require("graphql");
const fetch = require("node-fetch");
const Card = require("../mongo-models/card");
const Set = require("../mongo-models/set");
const Price = require("../mongo-models/price");
const Connector = require("../utils/mkmConnector");

const MKM_URL = "https://api.cardmarket.com/ws/v2.0/output.json/";

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLSchema,
  GraphQLID,
  GraphQLFloat,
  GraphQLList,
} = graphql;

function getInit(url) {
  return Connector.getMkmHeader(url);
}
function fetchResponseByURL(relativeURL) {
  const url = new URL(`${MKM_URL}${relativeURL}`);
  return fetch(url, {
    method: "get",
    headers: getInit(url),
  })
    .then((res) => res.json())
    .catch((err) => console.log(err));
}
function fetchCardData({ name }) {
  const uri = `products/find?search=${name}&idGame=1&idLanguage=1`;
  return fetchResponseByURL(uri).then((json) => json.product);
}
function fetchCardDetails(idProduct) {
  const uri = `products/${idProduct}`;
  return fetchResponseByURL(uri).then((json) => json.product);
}

const CardType = new GraphQLObjectType({
  name: "Card",
  fields: () => ({
    id: {
      type: GraphQLID,
    },
    multiverseId: {
      type: GraphQLString,
      resolve: (obj) => obj?.identifiers?.multiverseId

    },
    mcmId: {
      type: GraphQLString,
      resolve: (obj) => obj.identifiers && obj.identifiers.mcmId
    },
    mtgjsonId: {
      type: GraphQLString,
      resolve: (obj) => obj.uuid
    },
    name: {
      type: GraphQLString,
    },
    scryfallIllustrationId: {
      type: GraphQLString,
      resolve: (obj) => obj.identifiers && obj.identifiers.scryfallIllustrationId
    },
    printingSet: {
      type: GraphQLString,
    },
    foreignName: {
      type: new GraphQLList(
        new GraphQLObjectType({
          name: "Foreign",
          fields: () => ({ name: { type: GraphQLString } }),
        })
      ),
      resolve(parent, arg) {
        return parent.foreignData.filter((data) => data.language === "French");
      },
    },
    prices: {
      type: PriceType,
      resolve(parent, args) {
        return Price.findOne({ slug: parent.uuid })
      }
    },
    set: {
      type: SetType,
      resolve(parent, args) {
        return Set.findOne({ code: parent.printingSet });
      },
    },
  }),
});

const PriceType = new GraphQLObjectType({
  name: "Price",
  fields: () => ({
    id: {
      type: GraphQLID,
    },
    slug: {
      type: GraphQLString,
    },
    today: {
      type: new GraphQLObjectType({
        name: "Today",
        fields: () => ({
          magiccardmarket: {
            type: new GraphQLObjectType({
              name: "Cardmarket",
              fields: () => ({
                value: {
                  type: GraphQLFloat,
                  resolve: (data) => {
                    const obj = data?.paper?.cardmarket?.retail?.normal || data?.paper?.cardmarket?.retail?.foil;
                    const lastPrice = obj ? Object.values(obj).pop() : 0;
                    return lastPrice;
                  }
                },
                currency: {
                  type: GraphQLString,
                  resolve: (data) => data?.paper?.cardmarket?.currency
                }
              }),
            }),
            resolve: (data) => {
              return data;
            }
          },
          tcgplayer: {
            type: new GraphQLObjectType({
              name: "Tcgplayer",
              fields: () => ({
                value: {
                  type: GraphQLFloat,
                  resolve: (data) => {
                    const obj = data?.paper?.tcgplayer?.retail?.normal || data?.paper?.tcgplayer?.retail?.foil;
                    const lastPrice = obj ? Object.values(obj).pop() : 0;
                    return lastPrice;
                  }
                },
                currency: {
                  type: GraphQLString,
                  resolve: (data) => data?.paper?.tcgplayer?.currency
                }
              }),
            }),
            resolve: (data) => {
              return data
            }
          },
        }),
      }),
      resolve: (data) => {
        return data
      }
    }
  })
})

const SetType = new GraphQLObjectType({
  name: "Set",
  fields: () => ({
    id: {
      type: GraphQLID,
    },
    name: {
      type: GraphQLString,
    },
    code: {
      type: GraphQLString,
    },
    cards: {
      type: new GraphQLList(CardType),
      resolve(parent, args) {
        return Card.find({
          printingSet: parent.code,
        });
      },
    },
  }),
});

const MkmProduct = new GraphQLObjectType({
  name: "MkmProduct",
  description: "Somebody that you used to know",
  fields: () => ({
    idProduct: {
      type: GraphQLID,
    },
    name: {
      type: GraphQLString,
      description: "What you yell at me",
      resolve: (obj) => obj.enName,
    },
    image: {
      type: GraphQLString,
      description: "Path to image",
    },
    priceGuide: {
      type: new GraphQLObjectType({
        name: "PriceGuide",
        fields: () => ({
          SELL: {
            type: GraphQLFloat,
            resolve: (product) => product?.priceGuide?.SELL
          }, // Average price of articles ever sold of this product
          LOW: {
            type: GraphQLFloat,
            resolve: (product) => product?.priceGuide?.LOW || 0
          }, // Current lowest non-foil price (all conditions)
          LOWEX: {
            type: GraphQLFloat,
            resolve: (product) => product?.priceGuide?.LOWEX || 0
          }, // Current lowest non-foil price (condition EX and better)
          LOWFOIL: {
            type: GraphQLFloat,
            resolve: (product) => product?.priceGuide?.LOWFOIL || 0
          }, // Current lowest foil price
          AVG: {
            type: GraphQLFloat,
            resolve: (product) => product?.priceGuide?.AVG || 0
          }, // Current average non-foil price of all available articles of this product
          TREND: {
            type: GraphQLFloat,
            resolve: (product) => product?.priceGuide?.TREND || 0
          }, // Current trend price
          TRENDFOIL: {
            type: GraphQLFloat,
            resolve: (product) => product?.priceGuide?.TRENDFOIL || 0
          },
        }),
      }),
      resolve: (product) => fetchCardDetails(product.idProduct), // Fetch the friends with the URLs `person.friends`,
    },
  }),
});

// User request
const RootQuery = new GraphQLObjectType({
  name: "RootQueryType",
  fields: {
    card: {
      type: CardType,
      args: {
        id: {
          type: GraphQLID,
        },
      },
      resolve(parent, args) {
        return Card.findById(args.id);
      },
    },
    searchCard: {
      args: {
        name: {
          type: GraphQLString,
        },
      },
      type: new GraphQLList(CardType),
      resolve(parent, args) {
        return Card.find({
          $or: [
            { name: new RegExp(`^${args.name}`, "i") },
            {
              foreignData: {
                $elemMatch: {
                  name: new RegExp(`^${args.name}`, "i"),
                  language: "French",
                },
              },
            },
          ],
        });
      },
    },
    searchSet: {
      args: {
        code: {
          type: GraphQLString,
        },
      },
      type: new GraphQLList(SetType),
      resolve(parent, args) {
        return Set.find({ name: args.code });
      },
    },
    set: {
      type: SetType,
      args: {
        id: {
          type: GraphQLID,
        },
      },
      resolve(parent, args) {
        return Set.findById(args.id);
      },
    },
    cards: {
      type: new GraphQLList(CardType),
      resolve(parent, args) {
        return Card.find({});
      },
    },
    sets: {
      type: new GraphQLList(SetType),
      resolve(parent, args) {
        return Set.find({});
      },
    },
    mcmCards: {
      //allcard
      type: new GraphQLList(MkmProduct),
      args: {
        name: {
          type: GraphQLString,
        },
      },
      resolve: (root, arg) => fetchCardData(arg), // Fetch the idProduct of card from the REST API,
    },
    mcmCardData: {
      // card
      type: MkmProduct,
      args: {
        idProduct: {
          type: GraphQLID,
        },
      },
      resolve: (parent, args) => fetchCardDetails(args.idProduct), // Fetch the product with ID `args.id`,
    },
  },
});

module.exports = new GraphQLSchema({
  query: RootQuery,
});
