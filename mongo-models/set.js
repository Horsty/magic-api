const mongo = require("mongoose");
const Schema = mongo.Schema;

const setSchema = new Schema({
  name: String,
  code: String,
});

module.exports = mongo.model("Set", setSchema);
