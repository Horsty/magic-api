const mongo = require("mongoose");
const Schema = mongo.Schema;

const cardSchema = new Schema({
  name: String,
  uuid: String,
  identifiers: {
    multiverseId: Number,
    mcmId: Number,
    scryfallIllustrationId: String,
  },
  printings: [String],
  foreignData: [
    {
      name: String,
      language: String,
    },
  ],
});

module.exports = mongo.model("Card", cardSchema);
