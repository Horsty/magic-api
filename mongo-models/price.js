const mongo = require("mongoose");
const Schema = mongo.Schema;

const priceSchema = new Schema({
    slug: String, // id card
    paper: {
        cardmarket: {
            retail: {
                normal: {
                },
                foil: {
                },
            },
            lastPrice: Number,
            currency: String
        },
        tcgplayer: {
            retail: {
                normal: {
                },
                foil: {
                },
            },
            lastPrice: Number,
            currency: String
        }
    }
})

module.exports = mongo.model("Price", priceSchema);