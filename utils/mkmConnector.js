/*
 * This is a Pre-request script for Postman client to remediate OAuth 1.0a issue
 * where certain request fails if it has a query parameter that includes some special characters.
 *
 * NOTE: This Pre-script is only available for "GET" request.
 * In order to use this Pre-request script, you need to change your "Authorization" type to
 * "No Auth" only for the target request and do not apply to the top-level object.
 */
const CryptoJS = require("crypto-js");

const getMkmHeader = (url) => {
  const oauth_consumer_key = process.env.APP_TOKEN;
  const oauth_consumer_secret = process.env.APP_SECRET;

  const oauth_token = "";
  const oauth_secret = "";
  const oauth_signing_key = `${oauth_consumer_secret}&${oauth_secret}`;

  // create random oauth_nonce string
  const random_source =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  let oauth_nonce = "";
  for (let i = 0; i < 32; i++) {
    oauth_nonce += random_source.charAt(
      Math.floor(Math.random() * random_source.length)
    );
  }

  const oauth_parameter_string_object = {};
  oauth_parameter_string_object.oauth_consumer_key = oauth_consumer_key;
  oauth_parameter_string_object.oauth_token = oauth_token;
  oauth_parameter_string_object.oauth_nonce = oauth_nonce;
  oauth_parameter_string_object.oauth_timestamp = Math.round(
    new Date().getTime() / 1000
  );
  oauth_parameter_string_object.oauth_signature_method = "HMAC-SHA1";
  oauth_parameter_string_object.oauth_version = "1.0";

  // for Authorization request header (copy object)
  const oauth_authorization_header_object = {};
  for (let key in oauth_parameter_string_object) {
    oauth_authorization_header_object[key] = oauth_parameter_string_object[key];
  }
  // convert query string into object (+ encode)
  const searchParams = url && url.searchParams;
  let url_query_string_object =
    searchParams && Object.fromEntries(searchParams.entries());

  // parse request.params
  for (var key in url_query_string_object) {
    oauth_parameter_string_object[key] = url_query_string_object[key];
  }

  // sort object by key
  const oauth_parameter_string_object_ordered = {};
  Object.keys(oauth_parameter_string_object)
    .sort()
    .forEach(function (key) {
      oauth_parameter_string_object_ordered[key] =
        oauth_parameter_string_object[key];
    });

  // convert object into array
  const oauth_parameter_string_array = [];
  for (var key in oauth_parameter_string_object_ordered) {
    oauth_parameter_string_array.push(
      `${key}=${oauth_parameter_string_object_ordered[key]}`
    );
  }

  // generate parameter string
  const oauth_parameter_string = oauth_parameter_string_array.join("&");

  // replace dynamic variables
  let base_host = url && `${url.origin}${url.pathname}`;

  // generate base string
  const oauth_base_string = `GET&${encodeURIComponent(
    base_host
  )}&${encodeURIComponent(oauth_parameter_string)}`;

  // generate signature
  const oauth_signature = CryptoJS.enc.Base64.stringify(
    CryptoJS.HmacSHA1(oauth_base_string, oauth_signing_key)
  );

  oauth_authorization_header_object.oauth_signature = oauth_signature;
  // convert object into array (for Authorization header string)
  const oauth_authorization_header_array = [];
  for (var key in oauth_authorization_header_object) {
    oauth_authorization_header_array.push(
      `${key}="${oauth_authorization_header_object[key]}"`
    );
  }
  oauth_authorization_header_array.unshift(`realm="${base_host}"`);
  const oauth_authorization_header = oauth_authorization_header_array.join(
    ", "
  );

  // generate Authorization header
  return {
    Authorization: "OAuth " + oauth_authorization_header,
  };
};

module.exports = {
  getMkmHeader,
};
