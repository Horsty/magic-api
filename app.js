const express = require("express");
const graphqlHTTP = require("express-graphql");

const cors = require("cors");
const schema = require("./schema/schema");
const mongo = require("mongoose");
const mongoConnectString = process.env.NODE_ENV === 'production' ? process.env.MONGO_DB_CONNEXION_STRING : 'mongodb://localhost:27017/magic-tools';
const app = express();

const mkmHeader = require("./utils/mkmConnector");

const header = mkmHeader.getMkmHeader();

mongo.connect(mongoConnectString, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
mongo.connection.once("open", () => {
  console.log("connected to database");
});
app.use(cors());
app.use(
  "/graphql",
  graphqlHTTP({
    schema,
    graphiql: true,
  })
);
const port = process.env.PORT || 8080;
app.listen(port, () => {
  console.log(`Server running successfully on http://localhost:${port}/graphql ...`);
});
